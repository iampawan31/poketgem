<?php

class IndexController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		$data = array(
			'title' => 'Poketgem - Homepage');
		return View::make('index', $data);
	}



}
